# Jungle 
====
> Version 1.0.1\
> CMPS 183 Fall 2018 project.\
> Because the Amazon Jungle has many different species of animals.

## Team Members
|                    |                   |
|--------------------|-------------------|
|Randall Li          | rhli@ucsc.edu     |
|Zachary Harris      | zkharris@ucsc.edu |
|Carly Xie           | cxie2@ucsc.edu    |

## Technology

### Project Management
* Trello - Online Scrum board
* Github - Software Management

### Frameworks/Libraries/APIs
* VueJS
* Web2py

## Getting Started

### Dependencies
`NodeJS`
`yarn`

### Set up
1. Install NodeJS and Yarn.
2. After cloning the repo to your local machine, install the dependencies described in the package.json, by using your package manager. If you are using yarn, run this command in the project's directory:
`yarn install`
3. Run the application.
`yarn run dev`

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
